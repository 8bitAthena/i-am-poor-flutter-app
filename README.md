# I Am Poor Flutter App

## Project Summary

A basic flutter app based on the idea of the "I Am Rich" ios app released  
in 2008. The app was priced at the maximum allowed on the app store at the time,  
$999.99.  
It had minimal functionality and displays an icon of a ruby when opened. It was  
used as a status symbol to show people how absolutely balling you were.

My app is called I am poor and based on the recent crypto crashes shows the  
logo for bitcoin.
It can be used to identify fellow crypt-bros who are also financially suffering  
and is free except for the price of your pride.

## Prerequisits and How to Run

Must have android studio installed with flutter and dart plugins.
Open using android studio and run using the device manager to launch an emulator,  
I tested on a Pixel 6 with API 30.