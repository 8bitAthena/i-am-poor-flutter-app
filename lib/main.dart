import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.pink[50],
        appBar: AppBar(
          backgroundColor: Colors.deepPurpleAccent,
          title: const Center(
            child: Text('I Am Poor'),
          ),
        ),
        body: const Center(
          child: Image(
            image: AssetImage('images/bcIcon.png'),
          ),
        ),
      ),
    ),
  );
}
